export function flatten(elements) {
    let flat = [];
    for(let i = 0; i < elements.length; i++) {
        if(typeof elements[i] === 'object'){
            flat = flat.concat(flatten(elements[i]));
        }
        else {
            flat.push(elements[i]); 
        }
    }
    return flat; 
}

