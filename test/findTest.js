import {find} from '../find.js'; 

function callback(ele) {
    if(ele > 4){ 
        return true; 
    }
}

function test(func, input, expectedResult) {
    let output = func(input, callback); 
    console.log(`input: ${input}\nexpected: ${expectedResult}\noutput: ${output}`); 

    if(output === expectedResult){
        console.log('Test Passed'); 
    }
    else {
        console.log('Test Failed');
    }
}

test(find, [1, 2, 3, 4, 5, 5], 5); 