import {map} from '../map.js'; 

function callback(element, index) {
    return element**index; 
}

function test(func, input, expectedResult) {
    let output = func(input, callback); 
    console.log(`input: ${input}\nexpected: ${expectedResult}\noutput: ${output}`); 

    let check = 1; 
    for(let i = 0; i < expectedResult.length; i++) {
        if(output[i] !== expectedResult[i]) {
            check = 0; 
            break;    
        }
    }

    if(check){
        console.log('Test Passed'); 
    }
    else {
        console.log('Test Failed');
    }
}

test(map, [1, 2, 3, 4, 5, 5], [1, 2, 9, 64, 625, 3125]); 
