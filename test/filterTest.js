import {filter} from '../filter.js'; 

function callback(ele) {
    if(ele%2 === 0) return true;     
}

function test(func, input, expectedResult) {
    let output = func(input, callback); 
    console.log(`input: ${input}\nexpected: ${expectedResult}\noutput: ${output}`); 

    let check = 1; 
    if(expectedResult.length){
        for(let i = 0; i < expectedResult.length; i++) {
            if(output[i] !== expectedResult[i]) {
                check = 0; 
                break;    
            }
        }
    }
    else { // empty array expected
        if(output.length) check = 0;  // but output not empty
    }
    

    if(check){
        console.log('Test Passed\n'); 
    }
    else {
        console.log('Test Failed\n');
    }
}

test(filter, [1, 2, 3, 4, 5, 5], [2, 4]);
test(filter, [1, 3, 5, 5], []);