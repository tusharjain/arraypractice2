import {reduce} from '../reduce.js'; 

function callback(init, ele) {
    return init+ele; 
}

function test(func, input, expectedResult) {
    let output = func(input, callback); 
    console.log(`input: ${input}\nexpected: ${expectedResult}\noutput: ${output}`); 

    if(output === expectedResult){
        console.log('Test Passed'); 
    }
    else {
        console.log('Test Failed');
    }
}

test(reduce, [1, 2, 3, 4, 5, 5], 20); 