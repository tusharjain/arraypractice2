import {each} from '../each.js';

const items = [1, 2, 3, 4, 5, 5];
function callback(element, index) {
    console.log(`array[${index}] = ${element}`); 
}

each(items, callback);