import {flatten} from '../flatten.js'; 


function test(func, input, expectedResult) {
    let output = func(input); 
    console.log('input: ');
    console.log(input);

    console.log('expectedResult: ');
    console.log(expectedResult);

    console.log('output: ');
    console.log(output);

    let check = 1; 
    for(let i = 0; i < expectedResult.length; i++) {
        if(output[i] !== expectedResult[i]) {
            check = 0; 
            break;    
        }
    }

    if(check){
        console.log('Test Passed\n'); 
    }
    else {
        console.log('Test Failed\n');
    }
}

test(flatten, [1, [2], [[3], [[4]]]], [1, 2, 3, 4]);
test(flatten, [1, [5, 2], [[3], [[4, 8]]]], [1, 5, 2, 3, 4, 8]); 
